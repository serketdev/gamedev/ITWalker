extends Control
var players = {}
var player_name: String
var ip: String
var port
var curgamemode = ""
var curmap = "Map1"

func _ready():
	
	
	NetworkController.connect("connection_failed", self, "_on_connection_failed")
	NetworkController.connect("connection_succeeded", self, "_on_connection_success")
	NetworkController.connect("player_list_changed", self, "refresh_lobby")
	NetworkController.connect("gamemode_changed", self, "refresh_lobby")
	NetworkController.connect("game_ended", self, "_on_game_ended")
	NetworkController.connect("game_error", self, "_on_game_error")
	for i in range(len(MasterController.gamemodes)):
		$PlayerLobby/GamemodeSelect.add_item(MasterController.gamemodes[i], i)
	$PlayerLobby/GamemodeSelect.selected = 0
	$PlayerLobby/GamemodeSelect.text = MasterController.gamemodes[0]
	$PlayerLobby/GamemodeSelect.emit_signal("item_selected", 0)
func _gamemode_selected(gamemode):
	print(gamemode)
	curgamemode = MasterController.gamemodes[gamemode]
	NetworkController.set_gamemode(curgamemode)
	refresh_lobby()

func _on_join():
	var player_name: String = $Connect/NameEdit.text
	var ip: String = $Connect/IPEdit.text
	var port = NetworkController.DEFAULT_PORT
	if ':' in ip:
		port = int(ip.split(":")[1])
		ip = ip.split(":")[0]
	
	if !player_name:
		$Connect/ErrorLabel.text = "Invalid name!"
		return
	
	if not ip.is_valid_ip_address():
		$Connect/ErrorLabel.text = "Invalid IP address!"
		return
	
	if port < 1 or port > 65535:
		$Connect/ErrorLabel.text = "Invalid port!"
		return
	
	$Connect/ErrorLabel.text = ""
	$Connect/HostButton.disabled = true
	$Connect/JoinButton.disabled = true
	NetworkController.join_game(player_name, ip, port)
	#$Connect.hide()
	#$PlayerLobby.show()

func _on_host():
	player_name = $Connect/NameEdit.text
	ip = $Connect/IPEdit.text
	port = NetworkController.DEFAULT_PORT
	if ':' in ip:
		port = int(ip.split(":")[1])
		ip = ip.split(":")[0]
	
	if !player_name:
		$Connect/ErrorLabel.text = "Invalid name!"
		return
	
	if port < 1 or port > 65535:
		$Connect/ErrorLabel.text = "Invalid port!"
		return
	$Connect/ErrorLabel.text = ""
	$Connect.hide()
	NetworkController.host_game(player_name, port)
	refresh_lobby()
	$PlayerLobby.show()
	
	#$Connect.hide()


func refresh_lobby():
	var players = NetworkController.get_player_list()
	curgamemode = NetworkController.get_gamemode()
	$PlayerLobby/PlayerList.clear()
	$PlayerLobby/PlayerList.add_item(NetworkController.get_player_name() + " (You)")
	for p in players:
		$PlayerLobby/PlayerList.add_item(p)
	$PlayerLobby/StartButton.disabled = not get_tree().is_network_server()
	$PlayerLobby/GamemodeSelect.disabled = not get_tree().is_network_server()
	$PlayerLobby/GamemodeSelect.text = curgamemode

func _on_Start_pressed():
	if get_tree().is_network_server():
		var startevent = EventObject.new()
		startevent.name = "START"
		startevent.args = [curgamemode, "Map1"]
		MasterController.add_effect_event(startevent)
		MasterController.controller_call("EffectController", "effect_play", ["fade_in"])
		

func _on_connection_success():
	$Connect.hide()
	$PlayerLobby.show()

func _on_connection_failed():
	$Connect/HostButton.disabled = false
	$Connect/JoinButton.disabled = false
	$Connect/ErrorLabel.text = "Connection failed!"

func _on_game_ended():
	show()
	$Connect.show()
	$PlayerLobby.hide()
	$Connect/HostButton.disabled = false
	$Connect/JoinButton.disabled = false

func _on_game_error(errtxt):
	$ErrorPopup.dialog_text = errtxt
	$ErrorPopup.popup_centered_minsize()