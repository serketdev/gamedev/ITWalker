extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var lobbymenu = preload("../LobbyMenu/LobbyMenu.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	$Start.grab_focus()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event is InputEventMouseMotion:
		$cursor.rect_position = event.position


func start():
	var lobbynode = lobbymenu.instance()
	get_parent().add_child(lobbynode)
	queue_free()
	


func credits():
	pass # Replace with function body.


func leave():
	var exitevent = EventObject.new()
	exitevent.name = "EXIT"
	MasterController.add_effect_event(exitevent)
	MasterController.controller_call("EffectController", "effect_play", ["fade_in"])
