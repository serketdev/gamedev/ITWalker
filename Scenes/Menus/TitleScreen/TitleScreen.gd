extends Spatial
var px = false
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	MasterController.connect("event",self,"_event_received")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _event_received(event):
	pass


func _input(event):
	if event is InputEventKey and event.pressed and not event.is_echo() and event.scancode == KEY_TAB:
		px = !px
		MasterController.controller_call("ShaderController", "shader_state", ["Pixelize", px])