extends CPUParticles

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var t = 5
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	emitting = true

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	t -= delta
	if t <= 0:
		queue_free()
