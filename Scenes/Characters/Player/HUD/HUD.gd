extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var shoot_speed_mod = 1
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	$AnimationPlayer.playback_speed = 1 * shoot_speed_mod

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func shootanim():
	$AnimationPlayer.playback_speed = 1 * shoot_speed_mod
	$AnimationPlayer.play("shoot")
	