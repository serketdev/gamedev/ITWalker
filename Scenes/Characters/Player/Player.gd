extends "../MovingCharacter/MovingCharacterQuake.gd"
var plr_name = "Neo"
var can_shoot = true
var partobj = preload("ShotgunParticles/CPUShotgunParticles.tscn")
var partobjred = preload("ShotgunParticles/CPUShotgunParticlesRed.tscn")
var redmaterial = preload("res://Assets/Materials/shotgunparticlered.material")
var shotgunspeed = 2.0
const BASE_SHOTGUN_TIME = 0.65
onready var shotgunraycast: RayCast = $Helper/HeadPos/RayCast
func _ready():
	if is_network_master():
		$Helper/HeadPos/PlayerCamera.current = true
	else:
		$HUD.visible = false
	add_to_group("player")
	$HUD.shoot_speed_mod = shotgunspeed
	$ShotgunTimer.wait_time = BASE_SHOTGUN_TIME / shotgunspeed

func _handle_inputs():
	._handle_inputs()
	input_dict["fwd"] = Input.get_action_strength("forwards")
	input_dict["bwd"] = Input.get_action_strength("backwards")
	input_dict["left"] = Input.get_action_strength("left")
	input_dict["right"] = Input.get_action_strength("right")
	input_dict["jump"] = Input.is_action_pressed("jump")
	input_dict["crouch"] = Input.is_action_pressed("crouch")
	input_dict["sprint"] = Input.is_action_pressed("sprint")
	input_dict["look_up"] = Input.get_action_strength("look_up")
	input_dict["look_down"] = Input.get_action_strength("look_down")
	input_dict["look_left"] = Input.get_action_strength("look_left")
	input_dict["look_right"] = Input.get_action_strength("look_right")

func _input(ev):
	if is_network_master():
		if ev is InputEventMouseMotion:
			var x = ev.relative.x
			var y = ev.relative.y
			#print("X" + str(x))
			#print("Y" + str(y))
			rotate_y(-deg2rad(float(x) * lastdelta * 5)) 
			helper.rotate_x(deg2rad(float(y) * lastdelta * 5))
			var rot = helper.get_rotation()
			rot.x = clamp(rot.x, -PI/2, PI/2)
			helper.set_rotation(rot)
			set_slave_rot()
			#print(get_rotation())
		if (ev.is_action_pressed("shoot_left") || ev.is_action_pressed("shoot_right")) && can_shoot:
			shoot(ev.is_action_pressed("shoot_left"))
			rpc("shoot", ev.is_action_pressed("shoot_left"))

puppet func shoot(mode):
	if is_network_master():
		$HUD.shootanim()
	can_shoot = false
	$ShotgunTimer.start()
	#$shotgunsound.play()
	if shotgunraycast.is_colliding():
		#$Particles.translate($Particles.global_transform.origin - $Helper/RayCast.get_collision_point())
		#get_tree().call_group("")	
		#$Particles.emitting = true
		#$Particles.emitting = false
		var part: CPUParticles
		if mode:
			part = partobj.instance()
		else:
			part = partobjred.instance()
		part.set_owner(get_tree().get_root().get_child(1))
		part.global_transform.origin = shotgunraycast.get_collision_point()
		print(part)
		get_tree().root.get_child(0).add_child(part)
		var colbody = shotgunraycast.get_collider()
		if colbody.is_in_group("enemy"):
			if mode:
				colbody.hit(false)
			else:
				colbody.hit(true)

func _on_ShotgunTimer_timeout():
	can_shoot = true

func set_player_name(newname):
	plr_name = newname