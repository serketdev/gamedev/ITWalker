extends KinematicBody
#Physics variables
var FLOOR_ANGLE_TOLERANCE = 45
var GRAVITY = -9.8
var GRAVITY_SCALE = 1
var WALK_MAX_SPEED = 7
var JUMP_HEIGHT = 1.5
var JUMP_MAX_AIRBORNE_TIME = 0.2
var SPRINT_SPEED_MULTIPLIER = 2
var CROUCH_SPEED_MULTIPLIER = 0.5
var ACCEL = 8
var DEACCEL = 20
var character_velocity: Vector3 = Vector3()
var dir: Vector3 = Vector3()
var on_air_time = 100
var jumping = false
var prev_jump_pressed = false
var cur_sprint_mult
var cur_crouch_mult
#Input variables

puppet var input_dict = {
	"fwd": 0,
	"bwd": 0,
	"left": 0,
	"right": 0,
	"jump": 0,
	"crouch": 0,
	"sprint": 0,
	"look_up": 0,
	"look_down": 0,
	"look_left": 0,
	"look_right": 0
}
var input_dir = Vector3()
#Other variables
onready var helper: Spatial = $Helper
onready var standposnode: Position3D = $StandingHelperPos
onready var crouchposnode: Position3D = $CrouchingHelperPos
var lastdelta = 0
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	if is_network_master():
		var postimer = Timer.new()
		postimer.connect("timeout", self, "set_slave_state")
		postimer.one_shot = false
		postimer.wait_time = 0.1
		add_child(postimer)
		postimer.start()
		


func _physics_process(delta):
	lastdelta = delta
	if is_network_master():
		_handle_inputs()
		rset_unreliable("input_dict", input_dict)
	cur_sprint_mult = SPRINT_SPEED_MULTIPLIER if input_dict["sprint"] else 1
	cur_crouch_mult = CROUCH_SPEED_MULTIPLIER if input_dict["crouch"] else 1
	input_dir = Vector3()
	input_dir.x += float(input_dict["left"]) - float(input_dict["right"])
	input_dir.z += float(input_dict["fwd"]) - float(input_dict["bwd"])
	var input_look_x = input_dict["look_right"] - input_dict["look_left"]
	var input_look_y = input_dict["look_down"] - input_dict["look_up"]
	
	rotate_y(-deg2rad(float(input_look_x) * delta * 100)) 
	helper.rotate_x(deg2rad(float(input_look_y) * delta * 100))
	
	var rot = helper.get_rotation()
	rot.x = clamp(rot.x, -PI/2, PI/2)
	helper.set_rotation(rot)
	#if Input.is_action_pressed("crouch"):
	#	helper.translation = helper.translation.linear_interpolate(crouchposnode.translation, delta * 10)
	#else:
	#	helper.translation = helper.translation.linear_interpolate(standposnode.translation, delta * 10)
	
	#handle movement
	_handle_movement()
	character_velocity = move_and_slide(character_velocity, Vector3(0,1,0), false, 4, deg2rad(FLOOR_ANGLE_TOLERANCE))
	

func _handle_movement():
	dir = Vector3()
	dir.x += float(input_dict["left"]) - float(input_dict["right"])
	dir.z += float(input_dict["fwd"]) - float(input_dict["bwd"])
	var normaldir = dir.normalized()
	if dir.length() > 1:
		dir = normaldir * 1
	var undir = dir
	dir = get_transform().basis.xform(dir)
	var hcharacter_velocity: Vector3 = character_velocity
	hcharacter_velocity.y = 0
	var target = dir
	target *= WALK_MAX_SPEED * cur_sprint_mult * cur_crouch_mult
	var accel
	if dir.dot(hcharacter_velocity.normalized()) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	if not is_on_floor():
		accel /= 10
	hcharacter_velocity = hcharacter_velocity.linear_interpolate(target, accel * lastdelta)
	character_velocity.x = hcharacter_velocity.x
	character_velocity.z = hcharacter_velocity.z
	character_velocity.y += GRAVITY * GRAVITY_SCALE * lastdelta
	if is_on_floor():
		on_air_time = 0
	#print(on_air_time)
	if jumping and character_velocity.y < 0:
		# If falling, no longer jumping
		jumping = false
	if on_air_time < JUMP_MAX_AIRBORNE_TIME and input_dict["jump"] and not prev_jump_pressed and not jumping:
		# Jump must also be allowed to happen if the character left the floor a little bit ago.
		# Makes controls more snappy.
		character_velocity.y = sqrt(-2*GRAVITY*GRAVITY_SCALE*JUMP_HEIGHT)# / sqrt(1.5) #h = v^2/2g; v = sqrt(2gh)
		jumping = true
	
	on_air_time += lastdelta
	prev_jump_pressed = input_dict["jump"]

func _handle_inputs():
	#handle inputs here
	input_dict["fwd"] = 0
	input_dict["bwd"] = 0
	input_dict["left"] = 0
	input_dict["right"] = 0
	input_dict["jump"] = 0
	input_dict["crouch"] = 0
	input_dict["sprint"] = 0
	input_dict["look_up"] = 0
	input_dict["look_down"] = 0
	input_dict["look_left"] = 0
	input_dict["look_right"] = 0

# network funcs
func set_slave_state():
	rpc_unreliable("get_master_state", translation, rotation, helper.rotation, character_velocity)
	#print("timer")

puppet func get_master_state(pos, rot, hrot, vel):
	translation = pos
	rotation = rot
	helper.rotation = hrot
	character_velocity = vel

	#print("got pos "+str(pos))

func set_slave_rot():
	rpc_unreliable("get_master_rot", rotation, helper.rotation)

puppet func get_master_rot(rot, hrot):
	rotation = rot
	helper.rotation = hrot