extends "MovingCharacter.gd"

#credit to https://github.com/WiggleWizard/quake3-movement-unity3d/blob/master/CPMcharacter.js
# frame occuring factors
var QUAKE_GRAVITY = 20.0
var QUAKE_FRICTION = 6.0

# movement stuff
var QUAKE_MOVESPEED = 7.0 # Ground movespeed
var QUAKE_RUNACCELERATION = 20.0 # Ground acceleration
var QUAKE_RUNDEACCELERATION = 18.0 # Ground deacceleration
var QUAKE_AIRACCELERATION = 2.0 # Air acceleration
var QUAKE_AIRDEACCELERATION = 2.0 # Deacceleration experienced when opposite strafing
var QUAKE_AIRCONTROL = 0.3 # How precise air control is
var QUAKE_SIDESTRAFEACCELERATION = 50.0 # How fast acceleration occurs to get up to sideStrafeSpeed when side strafing
var QUAKE_SIDESTRAFESPEED = 1.0 # Max speed to generate when side strafing
var QUAKE_JUMPSPEED = 8.0 # Jump speed
var QUAKE_HOLDJUMPTOBHOP = true # Holding down jump allows you to bhop

# vector stuff
var move_direction = Vector3()
var move_direction_norm = Vector3()
#var character_velocity = Vector3()
var character_top_velocity = 0.0

# is on ground?
var grounded = true

# queue jump?
var wish_jump = false

# real time friction value
var character_friction = 0.0

# was jump pressed?
var last_jump_pressed = false

func _handle_movement():
	grounded = is_on_floor()
	queue_jump()
	if grounded:
		ground_move()
	else:
		air_move()
	

# Queues the next jump like in Q3
func queue_jump():
	if QUAKE_HOLDJUMPTOBHOP:
		wish_jump = input_dict["jump"]
		return
	
	if input_dict["jump"] and !last_jump_pressed and !wish_jump:
		wish_jump = true
	if !input_dict["jump"] and last_jump_pressed:
		wish_jump = false
	last_jump_pressed = input_dict["jump"]

# Executes when the character is in the air
func air_move():
	var wish_dir = Vector3()
	var wish_vel: float = QUAKE_AIRACCELERATION
	var accel: float
	wish_dir = transform.basis.xform(input_dir)
	
	var wish_speed = wish_dir.length()
	wish_speed *= QUAKE_MOVESPEED
	
	wish_dir = wish_dir.normalized()
	move_direction_norm = wish_dir
	
	# Air control
	var wish_speed2 = wish_speed
	if character_velocity.dot(wish_dir) < 0:
		accel = QUAKE_AIRDEACCELERATION
	else:
		accel = QUAKE_AIRACCELERATION
	
	if input_dir.z == 0 and input_dir.x != 0: # If the character is ONLY strafing
		if wish_speed > QUAKE_SIDESTRAFESPEED:
			wish_speed = QUAKE_SIDESTRAFESPEED
		accel = QUAKE_SIDESTRAFEACCELERATION
	
	accelerate(wish_dir, wish_speed, accel)
	if QUAKE_AIRCONTROL:
		air_control(wish_dir, wish_speed2)
	
	character_velocity.y -= QUAKE_GRAVITY * GRAVITY_SCALE * lastdelta

# Air control occurs when the character is in the air,
# it allows characters to move side to side much faster
# rather than being 'sluggish' around corners.
func air_control(wish_dir, wish_speed):
	var zspeed: float
	var speed: float
	var dot: float
	var k: float
	var i: int
	
	if input_dir.z == 0 or wish_speed == 0: # Can't control if not moving bwd/fwd
		return
	
	zspeed = character_velocity.y
	character_velocity.y = 0.0
	# IDTech VectorNormalize()
	speed = character_velocity.length()
	character_velocity = character_velocity.normalized()
	
	dot = character_velocity.dot(wish_dir)
	k = 32.0
	k *= QUAKE_AIRCONTROL * dot * dot * lastdelta
	
	# Change direction while slowing down
	if dot > 0:
		character_velocity.x = character_velocity.x * speed + wish_dir.x * k
		character_velocity.y = character_velocity.y * speed + wish_dir.y * k
		character_velocity.z = character_velocity.z * speed + wish_dir.z * k
		
		character_velocity = character_velocity.normalized()
		move_direction_norm = character_velocity
	
	character_velocity.x *= speed
	character_velocity.y = zspeed # note this line
	character_velocity.z *= speed

# Called every frame when character is on ground
func ground_move():
	
	
	var wish_dir = Vector3()
	var wish_vel = Vector3()
	
	# Do not apply friction if character is going to jump
	if !wish_jump:
		apply_friction(1.0 * cur_sprint_mult * cur_crouch_mult)
	else:
		apply_friction(0)
	wish_dir = transform.basis.xform(input_dir)
	wish_dir = wish_dir.normalized()
	move_direction_norm = wish_dir
	
	var wish_speed = wish_dir.length()
	wish_speed *= QUAKE_MOVESPEED * cur_sprint_mult * cur_crouch_mult
	
	accelerate(wish_dir, wish_speed, QUAKE_RUNACCELERATION)
	
	# reset gravity
	character_velocity.y = 0
	
	if wish_jump:
		character_velocity.y = sqrt(2*QUAKE_GRAVITY*GRAVITY_SCALE*JUMP_HEIGHT)
		wish_jump = false

func apply_friction(t):
	var vec = character_velocity
	var vel: float
	var speed: float
	var newspeed: float
	var control: float
	var drop: float
	
	vec.y = 0.0
	speed = vec.length()
	drop = 0.0
	
	# apply friction only if on ground
	if grounded:
		control = QUAKE_RUNDEACCELERATION if speed < QUAKE_RUNDEACCELERATION else speed
		drop = control * QUAKE_FRICTION * lastdelta * t
	
	newspeed = speed - drop
	character_friction = newspeed
	if newspeed < 0:
		newspeed = 0
	if speed > 0:
		newspeed /= speed
	
	character_velocity.x *= newspeed
	character_velocity.z *= newspeed

func accelerate(wish_dir, wish_speed, accel):
	var addspeed: float
	var accelspeed: float
	var currentspeed: float
	
	currentspeed = character_velocity.dot(wish_dir)
	addspeed = wish_speed - currentspeed
	if addspeed <= 0:
		return
	accelspeed = accel * lastdelta * wish_speed
	if accelspeed > addspeed:
		accelspeed = addspeed
	
	character_velocity.x += accelspeed * wish_dir.x
	character_velocity.z += accelspeed * wish_dir.z