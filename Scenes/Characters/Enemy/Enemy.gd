extends "../MovingCharacter/MovingCharacter.gd"
var Target
var isbad = false
var goodsprites = [preload("Sprites/agentsmithgood1.png"), preload("Sprites/agentsmithgood2.png")]
var badsprites = [preload("Sprites/agentsmithbad1.png"), preload("Sprites/agentsmithbad2.png"), preload("Sprites/agentsmithbad3.png")]
var material = preload("Materials/Enemy.material").duplicate()
func _ready():
	WALK_MAX_SPEED = 3
	randomize()
	add_to_group("enemy")
	Target = get_parent().Target
	MasterController.connect("event", self, "_event_received")
	isbad = bool(randi() % 2)
	if isbad:
		material.albedo_texture = badsprites[randi() % len(badsprites)]
	else:
		material.albedo_texture = goodsprites[randi() % len(goodsprites)]
	$MeshInstance2.set_surface_material(0, material)
func _physics_process(delta):
	if Target:
		look_at(Vector3(Target.transform.origin.x, transform.origin.y, Target.transform.origin.z), Vector3(0, 1, 0))
		rotate_y(PI)

func _handle_inputs():
	if Target:
		input_dict["fwd"] = 1.0

func _event_received(event):
	match event.name:
		"SETTARGET":
			Target = event.args[0]

func hit(bad):
	if bad == isbad:
		self.queue_free()