extends Control
enum EVENT {START, EXIT, GAMEOVER}
var effect_event_queue = [] #The holder array for events that are supposed to send after a certain effect ( Screen fade )
onready var Controllers = {
	"ShaderController" : $ShaderController,
	"EffectController" : $EffectController
}
var controllers_active = []
signal event
var cur_level_loaded
var gamemodes = []
# Called when the node enters the scene tree for the first time.
func _ready():
	# Gamemode list initialization
	var files = []
	var dir = Directory.new()
	dir.open("res://Scenes/Gamemodes")
	dir.list_dir_begin(true, true)
	while true:
		var file = dir.get_next()
		if file == "":
			break
		else:
			files.append(file)
	for i in range(len(files)):
		print("Gamemode: ", files[i])
		gamemodes.append(files[i])

	
	OS.window_position = Vector2(0,0)
	#OS.window_size = OS.get_screen_size() 
	#OS.window_size = Vector2(192,108)
	connect("event", self, "_event_received")
	for i in Controllers.keys():
		if Controllers[i]:
			controllers_active.append(i)
	if Controllers["EffectController"]:
		Controllers["EffectController"].connect("effect_finished", self, "_effect_finished")

func _effect_finished(effect: String):
	var cur_event = effect_event_queue.pop_front()
	if (cur_event):
		send_event(cur_event)

func preload_level(level):
	cur_level_loaded = load("res://Scenes/Levels/Level" + str(level) + "/Level.tscn")

#Events
#Current events for MasterController
func _event_received(event):
	match event.name:
		"START":
			print("starting")
			NetworkController.begin_game()
			#get_tree().change_scene("res://Scenes/Gamemodes/" + event.args[0] + "/Level" + str(event.args[1]) + "/Level.tscn")
		"EXIT":
			print("stopping...")
			get_tree().quit()
		"GAMEOVER":
			print("Game Over!")
			get_tree().change_scene("res://Scenes/Menus/GameOverScreen/GameOverScreen.tscn")
func add_effect_event(event):
	if Controllers["EffectController"]:
		effect_event_queue.append(event)
	else:
		send_event(event)

func send_event(event):
	emit_signal("event", event)

func level_loaded(level: String):
	print("level loaded")
	print(bool(Controllers["EffectController"].screen_obscured))
	if Controllers["EffectController"] != null:
		Controllers["EffectController"].effect_play("fade_out")

#func set_shader_state(shader, state):
#	if Controllers["ShaderController"]:
#		Controllers["ShaderController"].shader_state(shader, state)

func controller_call(controller: String, function: String, args: Array):
	if Controllers[controller]:
		Controllers[controller].callv(function, args)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

