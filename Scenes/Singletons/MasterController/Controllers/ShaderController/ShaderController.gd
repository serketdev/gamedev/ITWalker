extends Control
var shaders = []


func _ready():
	for i in get_children():
		shaders.append(i.name)


func shader_state(shader: String, state: bool) -> void:
	get_node("Shaders/" + shader).visible = state


func shader_turn_on_single(shader: String) -> void:
	for i in get_children():
		i.visible = false
	get_node("Shaders/" + shader).visible = true


func shader_turn_off():
	for i in get_children():
		i.visible = false