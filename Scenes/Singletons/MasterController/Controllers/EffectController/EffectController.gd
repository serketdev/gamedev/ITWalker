extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var effect_playing = false
var screen_obscured = false
var obscuring_effects = ["fade_in"]
var unobscuring_effects = ["fade_out"]
signal effect_finished
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func effect_play(effect: String):
	$EffectAnimationPlayer.play(effect)


func _effect_finished(effect):
	emit_signal("effect_finished", effect)
	$EffectElements.mouse_filter = MOUSE_FILTER_IGNORE
	effect_playing = false
	if effect in obscuring_effects:
		screen_obscured = true
	if effect in unobscuring_effects:
		screen_obscured = false
	
	#print(effect)


func _effect_started(effect):
	effect_playing = true
	$EffectElements.mouse_filter = MOUSE_FILTER_STOP
