extends Node2D
const DEFAULT_PORT = 4242 # Default port
const MAX_PEERS = 10 # Max players
var player_name = "Goku" # Player name
var players = {} # Player dictionary


signal player_list_changed()
signal gamemode_changed()
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(error)
remote var gamemode = ""
remote var map = "Map1"

func _player_connected(id):
	# This is not used in this demo, because _connected_ok is called for clients
	# on success and will do the job.
	pass

func _player_disconnected(id):
	if (get_tree().is_network_server()):
		if (has_node("/root/Game")): # Game in progress
			emit_signal("game_error", "Player " + players[id] + " disconnected")
			end_game()
		else: # Game not in progress
			unregister_player(id)
			for p_id in players:
				rpc_id(p_id, "unregister_player", id)

# Only for clients
func _connected_ok():
	# Tell everyone that we connected
	rpc("register_player", get_tree().get_network_unique_id(), player_name)
	emit_signal("connection_succeeded")

# Only for clients
func _server_disconnected():
	emit_signal("game_error", "Server disconnected")
	end_game()

# Only for clients
func _connected_fail():
	get_tree().set_network_peer(null)
	emit_signal("connection_failed")

# Lobby management

remote func register_player(id, new_player_name):
	print("Registering player ",id)
	if get_tree().is_network_server():
		rpc_id(id, "register_player", 1, player_name)
		for peer_id in players:
			rpc_id(id, "register_player", peer_id, players[peer_id])
			rpc_id(peer_id, "register_player", id, new_player_name)
		rpc_id(id, "set_gamemode", gamemode, 1)
	players[id] = new_player_name
	emit_signal("player_list_changed")

remote func unregister_player(id):
	players.erase(id)
	emit_signal("player_list_changed")

remote func pre_start_game(spawn_points, ngamemode, nmap):
	# Change scene
	var scenestr = "res://Scenes/Gamemodes/" + ngamemode +"/Maps/" + ngamemode + "_" + nmap + "/"  + ngamemode + "_" + nmap + ".tscn"
	print(scenestr)
	var world = load(scenestr).instance()
	get_tree().get_root().add_child(world)
	#get_tree().get_root().get_node("/root/LobbyMenu/").hide()
	get_tree().get_root().get_node("/root/TitleScreen/").hide()
	var player_scene = load("res://Scenes/Characters/Player/Player.tscn")
	print(spawn_points)
	for p_id in spawn_points:
		var spawn_pos = world.get_node("/root/MapSpatial/PlayerSpawnpoints/" + str(spawn_points[p_id])).translation
		var player = player_scene.instance()
		
		player.set_name(str(p_id))
		player.translation = spawn_pos
		player.set_network_master(p_id)
		
		if (p_id == get_tree().get_network_unique_id()):
			# if node for this peer id, set name
			player.set_player_name(player_name)
		else:
			# set name from peer
			player.set_player_name(players[p_id])
		
		world.add_child(player)
		
		if (not get_tree().is_network_server()):
			# Tell server we are ready to start
			rpc_id(1, "ready_to_start", get_tree().get_network_unique_id())
		elif players.size() == 0:
			post_start_game()
	
remote func post_start_game():
	get_tree().set_pause(false)

var players_ready = []

remote func ready_to_start(id):
	assert(get_tree().is_network_server())
	
	if not id in players_ready:
		players_ready.append(id)
	
	if players_ready.size() == players.size():
		for p in players:
			rpc_id(p, "post_start_game")
		post_start_game()

func host_game(new_player_name, port=DEFAULT_PORT):
	player_name = new_player_name
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, 10)
	get_tree().set_network_peer(peer)
	print(str(get_tree().get_network_unique_id()))
	

func join_game(new_player_name, ip, port=DEFAULT_PORT):
	player_name = new_player_name
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(ip, port)
	get_tree().set_network_peer(peer)

func get_player_list():
	return players.values()

func get_player_name():
	return player_name

func get_gamemode():
	return gamemode

remote func set_gamemode(new_gamemode, net_call = 0):
	print("set gamemode call with net ", net_call)
	if !net_call:
		rpc("set_gamemode", new_gamemode, 1)
		set_gamemode(new_gamemode, 1)
		print("set gamemode rpc ", new_gamemode)
	else:
		gamemode = new_gamemode
		print("gamemode set to ", gamemode)
	emit_signal("gamemode_changed")

func begin_game():
	assert(get_tree().is_network_server())
	
	var spawn_points = {}
	spawn_points[1] = 0
	var spawn_point_idx = 1
	for p in players:
		spawn_points[p] = spawn_point_idx
		spawn_point_idx += 1
	
	# call to pre-start game with spawnpoints
	for p in players:
		rpc_id(p, "pre_start_game", spawn_points, gamemode, map)
	
	pre_start_game(spawn_points, gamemode, map)

func end_game():
	if has_node("/root/Game"):
		get_node("/root/Game").queue_free()
	
	emit_signal("game_ended")
	players.clear()
	get_tree().set_network_peer(null)

func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

