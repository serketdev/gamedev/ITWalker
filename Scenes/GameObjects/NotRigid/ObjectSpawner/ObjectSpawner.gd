extends Path
export(PackedScene) var objectscene
export var spawn_time = 2
export var enabled = true
func _ready():
	randomize()
	$SpawnTimer.wait_time = spawn_time
	if enabled:
		turn_on()
	else:
		$SpawnTimer.one_shot = true

func turn_on():
	$SpawnTimer.start()
	$SpawnTimer.one_shot = false
	enabled = true

func turn_off():
	$SpawnTimer.one_shot = true

func _on_SpawnTimer_timeout():
	$PathFollow.offset = randi() / 100.0
	var obj = objectscene.instance()
	obj.translation = $PathFollow.translation
	get_parent().add_child(obj)
