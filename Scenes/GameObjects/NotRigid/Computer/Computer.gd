extends Area

func _ready():
	add_to_group("targetobject")


func _on_Computer_body_entered(body):
	if body.is_in_group("enemy"):
		var event = EventObject.new("GAMEOVER")
		MasterController.send_event(event)
