extends RigidBody

puppet var slave_rotation = Vector3()
puppet var slave_angular_velocity = Vector3()
puppet var slave_linear_velocity = Vector3()
puppet var slave_pos = Vector3()

puppet var slave_state = null
puppet var state_timer = 0
master var state_send_timer = 0

const ALPHA = 0.3
const EPSILON = 0.0005
const SCALE_FACTOR = 25
const STATE_EXPIRATION_TIME = 1.0 / 20.0
const STATE_SEND_TIME = STATE_EXPIRATION_TIME / 5

# Called when the node enters the scene tree for the first time.
func _ready():
	slave_rotation = rotation
	slave_angular_velocity = angular_velocity
	slave_linear_velocity = linear_velocity
	slave_pos = translation
	if get_tree().get_network_unique_id() == 1:
		var postimer = Timer.new()
		postimer.connect("timeout", self, "set_slave_state")
		postimer.one_shot = false
		postimer.wait_time = 0.1
		add_child(postimer)
		postimer.start()
	else:
		pass
func _physics_process(delta):
	pass

func _integrate_forces(state):
	if get_tree().get_network_unique_id() == 1:
		state_send_timer += state.step
		if state_send_timer > STATE_SEND_TIME:
			set_slave_state()
	else:
		if slave_state != null and state_timer < STATE_EXPIRATION_TIME:
			state_timer += state.step
			translation = lerp(translation, slave_state[0], 1.0 - ALPHA)
			rotation = slave_state[1]
			angular_velocity = slave_state[2]
			linear_velocity = slave_state[3]

func set_slave_state():
	var master_state = [translation, rotation, angular_velocity, linear_velocity]
	rpc_unreliable("get_master_state", master_state)
	state_send_timer = 0
	#print("timer")

puppet func get_master_state(to_state):
	state_timer = 0
	slave_state = to_state