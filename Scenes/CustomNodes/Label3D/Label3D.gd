tool
extends Spatial
class_name Label3D
export var text: String = "Text" setget set_label_text, get_label_text
export var FontSize: int = 24
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Viewport/CanvasLayer/Label.text = text


func set_label_text(value):
	text = value
	if $Viewport/CanvasLayer/Label:
		$Viewport/CanvasLayer/Label.text = value

func get_label_text():
	return text



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
