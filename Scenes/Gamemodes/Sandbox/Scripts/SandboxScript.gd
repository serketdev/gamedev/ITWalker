extends Spatial
var Target
func _ready():
	MasterController.level_loaded(name)
	MasterController.connect("event", self, "_event_received")


func _event_received(event):
	match event.name:
		"GAMEOVER":
			print("Game Over!")