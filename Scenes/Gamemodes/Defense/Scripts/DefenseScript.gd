extends Spatial
var Target
func _ready():
	MasterController.level_loaded(name)
	MasterController.connect("event", self, "_event_received")
	var target = get_tree().get_nodes_in_group("targetobject")[0]
	var event = EventObject.new("SETTARGET",[target])
	MasterController.add_effect_event(event)

func _event_received(event):
	match event.name:
		"GAMEOVER":
			print("Game Over!")
		"SETTARGET":
			Target = event.args[0]
		"GOAL":
			pass
		