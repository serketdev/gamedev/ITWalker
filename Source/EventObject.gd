extends Object
class_name EventObject
var name = ""
var args = []
func _init(argname = null, argargs = null):
	if argname:
		name = argname
	if argargs:
		args = argargs